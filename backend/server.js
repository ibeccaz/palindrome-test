const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', (req, res, next) => {
    res.sendStatus(200).end();
});

app.post('/', (req, res, next) => {
    if(req.body.text){
        var code = 200;
        
        let str = req.body.text.toLowerCase().replace(/\W/g, '').replace(/\s+/g,'');
        let strTest = str.split('').reverse().join('');

        if(strTest !== str){
            code = 400;
        }
        res.sendStatus(code).end();

    }else{
        res.status(500).send("Post can't be empty").end();
    }    
});

app.listen(3000, '127.0.0.1', () => console.log("Express Server Running on http://localhost:3000"));