import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  input = '';
  message = '';
  form;
  title = 'app';

  constructor(private formBuilder : FormBuilder, private http : Http){}
  ngOnInit(){
    this.builForm();
  }

  builForm(){
    this.form = this.formBuilder.group({
      text : this.formBuilder.control('')
    })
  }

  reStart() {
    this.builForm();
    this.message = '';
    this.input = '';
  }

  onSubmit(value) {
    if(value.text == ""){
      this.message = "Invalid input";
      return;
    }
    this.input = `${value.text}`;

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    this.http.post("http://localhost:3000", value, options)
             .subscribe(
              res => {                
                this.message = 'is a palindrome!';
              },
              err => {
                if(err.status == 400){
                  this.message = 'is NOT a palindrome!';
                }else{
                  this.input = '';
                  this.message = 'Server is not running or something...';
                }
            });
  }
}
