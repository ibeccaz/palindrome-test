**Requirements**

1. [NodeJS](https://nodejs.org/en/)
2. [Angular-cli](https://angular.io/guide/quickstart)

*Or you can skip the angular-cli thingy and just go for the build in the **/public** folder*


---

## Backend

First things first, start the server.

1. Open your terminal go to the project folder **cd /your/path/to/fullstack-challenge/**
2. Locate the **/backend** folder
3. Install all dependencies with the command **npm install**
4. And then run **node server.js** command
5. It should prompt a message saying the server is running at **http://localhost:3000** you can check that in the browser or using a tool like *Postman*
6. Do not close this tab.

---

## Frontend

Next, assuming you have installed [angular-cli](https://angular.io/guide/quickstart).

1. Open a new tab in your terminal.
2. Go to the project folder **cd /your/path/to/fullstack-challenge/frontend**
3. Install all dependencies with the command **npm install**, it might take a little while.
4. Once that's done, you can run the **ng serve** and go to **http://localhost:4200** or simply **ng serve --open** it should open the browser automatically.
5. That's it!

*If you have troubles with angular-cli make sure you are running ng commands inside the frontend folder*

---

## OR use the Frontend Build

If you have an apache server and would love to skip the angular-cli installation:

1. Copy the **/public** folder into your server
2. Go to your browser and try **http://your-localhost/fullstack-challenge/public/**
3. And that should do the trick.

*Obs: if you change the /public folder path don't forget to edit the <base href="/to-your-path/"> in the index.html otherwise it won't be able to locate the files*